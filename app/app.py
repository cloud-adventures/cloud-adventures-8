from flask import Flask, request, render_template
import joblib
import numpy as np
import os

import json
app = Flask(__name__)

with open("./artifacts/columns.json", 'r') as f:
    __data_columns = json.load(f)['data_columns']
    __postcodes = __data_columns[11:]
    __proptypes = __data_columns[3:11]

with open("./artifacts/model3.pkl", 'rb') as f:
    __model = joblib.load(f)

__bedrooms = [1, 2, 3, 4, 5]
__bathrooms = [1, 2, 3, 4, 5]
__receptions = [1, 2, 3]


@app.route('/')
def index():
    return render_template('app.html', proptypes=__proptypes,
                           postcodes=__postcodes,
                           bedrooms=__bedrooms,
                           bathrooms=__bathrooms, receptions=__receptions)


@app.route('/predict', methods=['POST'])
def predict():
    postcode = request.form.get('postcode')
    proptype = request.form.get('proptype')
    bed = int(request.form.get('bedrooms'))
    bath = int(request.form.get('bathrooms'))
    reception = int(request.form.get('reception'))

    try:
        loc_index = __data_columns.index(postcode)
        loc_index2 = __data_columns.index(proptype)
    except IndexError:
        loc_index, loc_index2 = -1, -1

    x = np.zeros(len(__data_columns))
    x[0] = bed
    x[1] = bath
    x[2] = reception
    x[loc_index] = 1
    x[loc_index2] = 1
    pred = int(np.expm1(__model.predict([x])[0]))

    return "{:,}".format(pred)


if __name__ == "__main__":
    print("Price prediction server starting..")
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
