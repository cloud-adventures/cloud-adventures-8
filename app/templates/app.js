function getBathValue() {
    var uiBathrooms = document.getElementsByName("uiBathrooms");
    for (var i in uiBathrooms) {
        if (uiBathrooms[i].checked) {
            return parseInt(i) + 1;
        }
    }
    return -1; // Invalid Value
}

function getBedValue() {
    var uiBedrooms = document.getElementsByName("uiBedrooms");
    for (var i in uiBedrooms) {
        if (uiBedrooms[i].checked) {
            return parseInt(i) + 1;
        }
    }
    return -1; // Invalid Value
}
function getReceptionValue() {
    var uiReception = document.getElementsByName("uiReception");
    for (var i in uiReception) {
        if (uiReception[i].checked) {
            return parseInt(i) + 1;
        }
    }
    return -1; // Invalid Value
}

function onClickedEstimatePrice() {
    console.log("Estimate price button clicked");
    var bedrooms = getBedValue();
    var bathrooms = getBathValue();
    var reception = getReceptionValue();
    var postcode = document.getElementById("uiPostcodes");
    var proptype = document.getElementById("uiProptype");
    var estPrice = document.getElementById("uiEstimatedPrice");

    var url = "http://127.0.0.1:5000/predict_price";
    $.post(url, {
        bed: bedrooms,
        bath: bathrooms,
        reception: reception,
        postcode: postcode.value,
        proptype: proptype.value
    }, function (data, status) {
        console.log(data.estimated_price);
        var price = Math.round(data.estimated_price)
        estPrice.innerHTML = "<h2>" + '&pound' + price.toLocaleString() + "</h2>";
        console.log(status);
    });
}

function onPageLoad() {
    console.log("document loaded");
    var url = "http://127.0.0.1:5000/get_proptype";
    $.get(url, function (data, status) {
        console.log("got response for get_proptype request");
        if (data) {
            var proptype = data.proptype;
            var uiProptype = document.getElementById("uiProptype");
            $('#uiProptype').empty();
            for (var i in proptype) {
                var opt = new Option(proptype[i]);
                $('#uiProptype').append(opt);
            }
        }
    });
    var url = "http://127.0.0.1:5000/get_postcodes";
    $.get(url, function (data, status) {
        console.log("got response for get_postcodes request");
        if (data) {
            var postcodes = data.postcodes;
            var uiLocations = document.getElementById("uiPostcodes");
            $('#uiPostcodes').empty();
            for (var i in postcodes) {
                var opt = new Option(postcodes[i]);
                $('#uiPostcodes').append(opt);
            }
        }
    });
}

window.onload = onPageLoad;