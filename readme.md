# Cloud Computing 8
<figure align = "center">
<img src="images/cloud8.png" alt="Trulli" width="800" height="423"">
</figure>


## Name
Flask App running on Bare Metal Kubernetes

## Description
Aims of project:
- Part 1: Implement Bare Metal Kubernetes on AWS.
- Part 2: Implement a Flask ML App
- Part 3: Run the Flask App on Kubernetes
- Part 4: Apply LoadBalancer with R53


## Results


### Part 1 Implement Bare Metal Kubernetes on AWS.
Following [this](https://techworld-with-nana.teachable.com/p/the-complete-kubernetes-course) guide build a Kubernets cluster from scratch on AWS EC2 instances.  Major tasks involve:
- TLS Certificates
- Configuring K8s nodes
- Container runtime
- Kubeadm, kubelet, kubectl and kubeconfig
- Networking and WeaveNet

Install cluster k8s on EC2 instances with master and two worker nodes.  Screenshot of the nodes is shown below.

<figure align = "center">
<img src="images/nodes1_small.png" alt="AWS " width="900" height="524"">
<figcaption align = "center"><b>AWS Diagram</b></figcaption>
</figure>

### Part 2 Flask App
Using the webscraped data from cloud-computing-7, build a Flask App that predicts house prices.  The source code is contained in the folder `app`.
- `artifacts` contains pickled model and dataframe data
- `static` and `templates` contain html, css and js files that build the front end
- `app.py` contains the Flask python source code

After testing locally, the app was dockerized and pushed to docker hub.  This image can then be pulled by k8s pods running in the Bare metal cluster.  

### Part 3 Run App on k8s
Build a deployment and service to expose the app.  Relevant files in `k8s` folder:
`flask-deployment.yaml` - run two pods containing the Flask app
`node-svc.yaml` - implement nodeport service using port 30000

<figure align = "center">
<img src="images/nodeport1.png" alt="AWS " width="1424" height="301"">
<figcaption align = "center"><b>Deployment and service running in cluster with kubectl</b></figcaption>
</figure>

The flask app accessed on one of the worker nodes using public IP address and port 30000 is show below:

<figure align = "center">
<img src="images/flask-node1_small.png" alt="AWS " width="900" height="524"">
<figcaption align = "center"><b>Deployment and service running in cluster</b></figcaption>
</figure>

### Part 4 Implement LoadBalancer and use R53 DNS

After using nodeport service, change to LoadBalancer service.  Use `svc-lb.yaml` to expose the Flask App.  Requires a Loadbalancer and target group built through AWS Console.

<figure align = "center">
<img src="images/lb2_small.png" alt="AWS " width="900" height="479"">
<figcaption align = "center"><b>LoadBalancer in AWS</b></figcaption>
</figure>

Register the two worker nodes as targets for the Loadbalancer
<figure align = "center">
<img src="images/lb4_small.png" alt="AWS " width="900" height="476"">
<figcaption align = "center"><b>Target group for LB.  Cluster worker nodes</b></figcaption>
</figure>

Use a Hosted Zone in R53 to give a clearer DNS name to the Loadbalancer.  Flask App shown below with my registered domain name `cloudrichard.com`

<figure align = "center">
<img src="images/dns2_small.png" alt="AWS " width="1000" height="573"">
<figcaption align = "center"><b>Loadbalanced Flask App with R53 hosted zone</b></figcaption>
</figure>


### To do


